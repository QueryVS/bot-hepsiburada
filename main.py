#!/usr/bin/python3
"""written by QueryVS

   License:GPLv1
   the app can scanning facebook user page.

"""
from hashlib import md5
from mymodules.datafetch import user
from os import mkdir
from shutil import rmtree
# import csv

url_lines = []


if (__name__ == "__main__"):
	with open("urls.lst","r") as f:
	    url_lines = f.readlines()
	with open('url-md5.csv','w') as f:
		csv_line = ""
		for i in range(0,len(url_lines),1):
			url_md5 = md5(url_lines[i].encode()).hexdigest()
			try:
				mkdir("output")
			except FileExistsError as err:
				rmtree("output")
				mkdir("output")
			mkdir("output/"+url_md5)
			mkdir("output/"+str(url_md5)+"/DOM")
			mkdir("output/"+str(url_md5)+"/OCR")
			user(url_lines[i],url_md5)
			if (url_lines[i] != ""):
				csv_line = url_lines[i] + url_md5  + "\n"
				f.writelines(csv_line)
			else:
				continue
else:
	print("this is not modules, can not be used as a module ")
