#!/usr/bin/python3
""" Written by QueryVS

"""
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import NoSuchElementException, TimeoutException
from selenium.webdriver.firefox.options import Options
from time import sleep


from mymodules.settings import BROWSER_EXE, FIREFOX_BINARY, GECKODRIVER, PROFILE

import sys
import calendar


class user:
	def __init__(self,url_line,url_md5,ids=["oxfess"], file="posts.csv", depth=5, delay=2):
		self.ids = ids
		self.out_file = file
		self.depth = depth + 1
		self.delay = delay
		self.url_line = url_line
		self.url_md5 = url_md5

		self.browser = webdriver.Firefox(executable_path=GECKODRIVER,
		                                 firefox_binary=FIREFOX_BINARY,
		                                 firefox_profile=PROFILE,)
		if(self.check_login() == True):
			print("process is success")
		else:
			print("process is success")

	def check_login(self):
		try:
			self.browser.get(self.url_line)
			sleep(5)
			self.browser.execute_script("window.scrollTo(0,document.body.scrollHeight)")

			try:
				page_body = self.browser.find_element_by_xpath('//*[@id="productresults"]')
			except NoSuchElementException as ex:
				#page_body = self.browser.find_element_by_xpath('')
				print(ex)

			page_body.screenshot("output/" + str(str(self.url_md5)+"/bot-hepsiburada_" + self.url_md5 + ".png"))

			self.browser.close()

			return True
		except Exception as ex:
			print(ex)
			return False

