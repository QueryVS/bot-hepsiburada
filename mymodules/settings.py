""" set geckodriver and firefox binary file 

	for use selenium
	forked from Apurv Mishra==> https://gitlab.com/QueryVS/facebook-scraper-selenium/-/blob/master/fb-scraper/utils.py

"""
import os
from selenium import webdriver
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from dotenv import load_dotenv, find_dotenv

load_dotenv(find_dotenv())


BROWSER_EXE = '/usr/bin/firefox-bin' #your firefox browser binary files
GECKODRIVER = './geckodriver' # geckodriver binary files ==> https://github.com/mozilla/geckodriver/releases
FIREFOX_BINARY = FirefoxBinary(BROWSER_EXE)

PROFILE = webdriver.FirefoxProfile()

# PROFILE.DEFAULT_PREFERENCES['frozen']['javascript.enabled'] = False

PROFILE.set_preference("dom.webnotifications.enabled", False)
PROFILE.set_preference("app.update.enabled", False)
PROFILE.update_preferences()
